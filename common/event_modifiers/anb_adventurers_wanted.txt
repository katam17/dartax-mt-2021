
#province modifiers related to Adventurers Wanted (formerly infestations) - try to keep them grouped together

aw_test_1 = {

}

aw_test_2 = {
	
}


aw_test_3 = {
	
}


aw_thieves_test_1 = {

}

aw_thieves_test_2 = {

}

aw_thieves_test_3 = {

}

aw_thieves_guild_1 = {
	local_tax_modifier = -0.1
	development_cost = 0.1

	available_province_loot = -0.25
}

aw_thieves_guild_2 = {
	local_tax_modifier = -0.25
	development_cost = 0.2

	available_province_loot = -0.5	#funnily enough, thieves guild will actually reduce loot for attackers to steal
}

aw_thieves_guild_3 = {
	local_tax_modifier = -0.5
	development_cost = 0.3
	global_tax_modifier = -0.01	#get fucked. lets go big with these AW, so try not to stack these!

	available_province_loot = -0.75
}

aw_thieves_guild_stolen_relics = {
	global_trade_goods_size_modifier = -0.5
}

aw_thieves_guild_war_among_thieves = {
	local_tax_modifier = -0.5
}


aw_bandits_1 = {
	local_monthly_devastation = 0.1
	province_trade_power_modifier = -0.1
}

aw_bandits_2 = {
	local_monthly_devastation = 0.2
	province_trade_power_modifier = -0.2
}

aw_bandits_3 = {
	local_monthly_devastation = 0.3
	province_trade_power_modifier = -0.3

	min_local_autonomy = 50
}


aw_bandits_tax_collectors_robbed = {
	local_tax_modifier = -0.25
	local_unrest = 5
}


aw_werewolves_1 = {
	supply_limit_modifier = -0.1
	attrition = 1
}

aw_werewolves_2 = {
	supply_limit_modifier = -0.2
	attrition = 2
}

aw_werewolves_3 = {
	supply_limit_modifier = -0.3
	attrition = 4
}

aw_werewolves_werewolf_hysteria = {
	local_unrest = 10
}

aw_werewolves_werewolf_hysteria_court_oversight = {
	local_unrest = 5
	local_governing_cost = 0.5
}


aw_trolls_1 = {
	local_monthly_devastation = 0.33
}

aw_trolls_2 = {
	local_monthly_devastation = 0.66
}

aw_trolls_3 = {
	local_monthly_devastation = 1
}

aw_trolls_troll_caused_famine = {
	supply_limit_modifier = -0.3
	development_cost = 0.5
}

aw_trolls_ate_local_militia = {
	local_defensiveness = -0.5
}

aw_trolls_bridge_troll_tolls = {
	supply_limit_modifier = 0.2
	local_friendly_movement_speed = 0.2
}


aw_gnolls_1 = {
	supply_limit_modifier = -0.1
	attrition = 1
}

aw_gnolls_2 = {
	supply_limit_modifier = -0.2
	attrition = 2
}

aw_gnolls_3 = {
	supply_limit_modifier = -0.3
	attrition = 4
}


aw_harpies_1 = {
	supply_limit_modifier = -0.1
	attrition = 1
}

aw_harpies_2 = {
	supply_limit_modifier = -0.2
	attrition = 2
}

aw_harpies_3 = {
	supply_limit_modifier = -0.3
	attrition = 4
}


aw_orcs_1 = {
	local_monthly_devastation = 0.2
}

aw_orcs_2 = {
	local_monthly_devastation = 0.4
}

aw_orcs_3 = {	#Migratory Clan
	supply_limit_modifier = -0.5
	local_governing_cost = 0.25
	local_monthly_devastation = 0.2
}

aw_orcs_looted_province = {
	available_province_loot = -1.0
}


aw_satyrs_1 = {
	local_unrest = -3
	local_production_efficiency = -0.33
}

aw_satyrs_2 = {
	supply_limit_modifier = -0.2
	attrition = 2
}

aw_satyrs_3 = {
	supply_limit_modifier = -0.3
	attrition = 4
}

aw_satyrs_scorned_lovers = {
	local_unrest = 7
}

aw_satyrs_hangover = {
	local_production_efficiency = -0.7
}

aw_satyrs_music = {
	local_unrest = -10
	local_tax_modifier = 0.25
}


aw_goblins_1 = {
	supply_limit_modifier = -0.1
	attrition = 1
}

aw_goblins_2 = {
	supply_limit_modifier = -0.2
	attrition = 2
}

aw_goblins_3 = {
	supply_limit_modifier = -0.3
	attrition = 4
}

aw_goblins_trade_disruption = {
	trade_steering = -0.1
}

aw_goblins_overrun = {
	local_defensiveness = -0.2
}

aw_ogres_1 = {
	supply_limit_modifier = -0.1
	attrition = 1
}

aw_ogres_2 = {
	supply_limit_modifier = -0.2
	attrition = 2
}

aw_ogres_3 = {
	supply_limit_modifier = -0.3
	attrition = 4
}


aw_zombies_1 = {
	supply_limit_modifier = -0.1
	attrition = 1
}

aw_zombies_2 = {
	supply_limit_modifier = -0.2
	attrition = 2
}

aw_zombies_3 = {
	global_unrest = 3
	global_tax_modifier = -0.25
	production_efficiency = -0.5
	development_cost = 0.5
	attrition = 5
}


aw_haunted_house_1 = {
	local_unrest = 3
}

aw_haunted_house_2 = {
	local_unrest = 6.66
}

aw_haunted_house_3 = {
	local_unrest = 6.66
}

aw_haunted_house_wilting_crops = {
	local_monthly_devastation = 0.5
	local_development_cost = 0.5
}

aw_haunted_house_too_scared_to_work = {
	local_production_efficiency = -0.5
	local_unrest = -5
}

aw_haunted_house_ghost_tours = {
	local_tax_modifier = 0.25
	local_unrest = 3
}



aw_ghouls_1 = {
	local_unrest = 10
}

aw_ghouls_2 = {
	local_unrest = 10
	attrition = 3
	local_friendly_movement_speed = -0.1
	local_hostile_movement_speed = -0.1
}

aw_ghouls_corpses_clog_sewers = {
	supply_limit_modifier = -0.25
	local_development_cost = 0.33
	local_unrest = 5
}

aw_ghouls_corpsefields = {
	local_unrest = 10
}



aw_wyvern_1 = {
	province_trade_power_modifier = -0.5
	local_unrest = 5
}

aw_wyvern_2 = {
	province_trade_power_modifier = -0.5
	local_unrest = 5
	attrition = 3
}

aw_wyvern_3 = {	#not used
	supply_limit_modifier = -0.3
	attrition = 4
}

aw_wyvern_attacks_on_caravans = {
	province_trade_power_modifier = -0.33
}

aw_wyvern_flights_scare_people = {
	local_unrest = 5
}


aw_wendigo_1 = {
	local_unrest = 2
}

aw_wendigo_2 = {
	attrition = 3
	local_manpower_modifier = -0.33
	local_monthly_devastation = 0.3
	supply_limit_modifier = -0.25

	stability_cost_modifier = 0.1	#this WILL stack
}

aw_wendigo_3 = {	#not used
	supply_limit_modifier = -0.3
	attrition = 4
}

aw_eldritch_cultists_1 = {
	local_unrest = 1
}

aw_eldritch_cultists_2 = {
	local_unrest = 2 # Cause some people aren't part of the cult and don't like it
	local_autonomy = 0.10
	local_production_efficiency = -0.10
}

aw_eldritch_cultists_3 = {
	local_unrest = -5 # Don't want to draw attention
	local_autonomy = 0.25 # These should be really unproductive
	local_production_efficiency = -0.10
	local_friendly_movement_speed = -0.10 # Makes people avoid it?
	local_hostile_movement_speed = -0.10
}

aw_eldritch_cultists_donation_spree = {
	local_tax_modifier = 0.50
}

aw_eldritch_cultists_sacrifices = {
	local_production_efficiency = -0.25
	local_unrest = 5
}

aw_eldritch_cultists_dead_livestock = {
	local_production_efficiency = -0.10
	local_monthly_devastation = 0.05
}